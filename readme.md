# Hboard <img src="app/src/main/ic_launcher-playstore.png" width="30px" alt="Icon"/>
The latest revolution in online communication: a keyboard that only lets you type the letter "h".

Comes with it's own accidentally on purpose inconsistent settings UI, just like Google's own apps.

> _This app won't be updated as frequently._

## Using Hboard
To use Hboard install it and set "Hboard" as your default input method

## Technical
The keyboard is view-based, while the settings UI is built with Jetpack Compose.

## Legal
This app is licensed under the GNU GPL, version 3.

This app also reuses some code from [Gugal](https://gitlab.com/narektor/gugal).