package com.porg.hboard.settings

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import com.porg.hboard.R
import com.porg.hboard.ui.theme.HboardTheme

class TryActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val that = this
        setContent {

            val content = remember {mutableStateOf("")}

            HboardTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    Column() {
                        UIComponents.Header(getString(R.string.settings_try), { finish() })

                        Column(modifier = Modifier.padding(10.dp)) {
                            Text(
                                getString(R.string.settings_try_desc_1),
                                fontWeight = FontWeight.Bold
                            )
                            Text(
                                getString(R.string.settings_try_desc_2),
                                fontStyle = FontStyle.Italic
                            )
                            TextField(
                                value = content.value,
                                onValueChange = {content.value = it},
                                modifier = Modifier.fillMaxWidth()
                            )
                        }
                    }
                }
            }
        }
    }
}