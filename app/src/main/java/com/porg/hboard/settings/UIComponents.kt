package com.porg.hboard.settings

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

// please do not use this
// this is intentionally made to be bad
class UIComponents {
    companion object {
        /**
         * A header with large text and optionally a Back button.
         *
         * @param text the title.
         * @param doFinish the action to do when the Back button is pressed, if the Back button is enabled. Can be null if the Back button isn't enabled.
         * @param showBackButton whether to show the Back button. Defaults to true.
         */
        // Adapted from Gugal: https://gitlab.com/narektor/gugal
        @Composable
        fun Header(text: String, doFinish: (() -> Unit)?, showBackButton: Boolean = true) {
            Column() {
                if (showBackButton)
                    IconButton(
                        onClick  = doFinish!!,
                        modifier = Modifier.
                        then(Modifier.padding(start = 16.dp, top = 16.dp, bottom = 0.dp, end = 16.dp))
                    ) {
                        Icon(
                            imageVector = Icons.Filled.ArrowBack,
                            contentDescription = "Go back",
                        )
                    }
                Text(
                    text = text,
                    modifier = Modifier
                        .padding(start = 24.dp, top = 72.dp, bottom = 24.dp, end = 24.dp)
                        .fillMaxWidth(),
                    style = MaterialTheme.typography.h4
                )
            }
        }

        @OptIn(ExperimentalMaterialApi::class)
        @Composable
        fun SettingsOption(text: String, desc: String? = null, onClick: () -> Unit) {
            Card(
                elevation = 0.dp,
                modifier = Modifier.fillMaxWidth(),
                onClick = onClick
            ) {
                Column (modifier = Modifier.padding(all = 20.dp)){
                    Text(text, fontSize = MaterialTheme.typography.h6.fontSize)
                    if (desc != null)
                        Text(desc, Modifier.padding(top=5.dp), style=MaterialTheme.typography.body1)
                }
            }
        }
    }
}