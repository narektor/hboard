package com.porg.hboard.settings

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.browser.customtabs.CustomTabsIntent
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.porg.hboard.BuildConfig
import com.porg.hboard.R
import com.porg.hboard.ui.theme.HboardTheme

class AboutActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val that = this
        setContent {
            HboardTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    Column() {
                        UIComponents.Header(getString(R.string.settings_about), { finish() })
                        Text(
                            "Hboard ${BuildConfig.VERSION_NAME}",
                            fontWeight = FontWeight.Bold,
                            style = MaterialTheme.typography.h4,
                            textAlign = TextAlign.Center,
                            modifier = Modifier.fillMaxWidth()
                        )
                        Text(
                            "(c) 2023 narektor",
                            style = MaterialTheme.typography.body1,
                            textAlign = TextAlign.Center,
                            modifier = Modifier.fillMaxWidth()
                        )
                        Divider(
                            modifier = Modifier
                                .fillMaxWidth()
                                .width(1.dp)
                                .padding(all = 16.dp)
                        )
                        UIComponents.SettingsOption(
                            getString(R.string.settings_about_src_title),
                            getString(R.string.settings_about_src_desc)
                        ) {
                            open(that, "https://gitlab.com/narektor/hboard")
                        }
                    }
                }
            }
        }
    }

    private fun open(context: Context, url: String) {
        val builder = CustomTabsIntent.Builder()
        // show website title
        builder.setShowTitle(true)
        // animation for enter and exit of tab
        builder.setStartAnimations(context, android.R.anim.fade_in, android.R.anim.fade_out)
        builder.setExitAnimations(context, android.R.anim.fade_in, android.R.anim.fade_out)
        // launch the passed URL
        builder.build().launchUrl(context, Uri.parse(url))
    }
}