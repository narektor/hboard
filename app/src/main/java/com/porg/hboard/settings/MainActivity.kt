package com.porg.hboard.settings

import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceActivity
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.porg.hboard.R
import com.porg.hboard.settings.UIComponents.Companion.SettingsOption
import com.porg.hboard.ui.theme.HboardTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val that = this
        setContent {
            HboardTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    Column() {
                        UIComponents.Header(getString(R.string.settings_name), { finish() })
                        SettingsOption(getString(R.string.settings_try)) {
                            startActivity(Intent(that, TryActivity::class.java))
                        }
                        SettingsOption(getString(R.string.settings_about)) {
                            startActivity(Intent(that, AboutActivity::class.java))
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name!")
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    HboardTheme {
        Greeting("Android")
    }
}

