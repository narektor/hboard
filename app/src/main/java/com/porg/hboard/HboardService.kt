package com.porg.hboard

import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.inputmethodservice.InputMethodService
import android.text.TextUtils
import android.view.View
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.Toast
import androidx.compose.material.Text
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import com.google.android.material.button.MaterialButton
import com.porg.hboard.settings.MainActivity

enum class HboardKey {
    H,
    SPACE,
    BACKSPACE,
    ENTER,
    MIC,
    SETTINGS
}

class HboardService : InputMethodService() {
    override fun onCreateInputView(): View {
        // get the KeyboardView and add our Keyboard layout to it
        return getXMLView()
    }

    fun getXMLView(): View {
        val keyboardView = layoutInflater.inflate(R.layout.hboard_view, null) as LinearLayout
        bind(keyboardView, R.id.key_space, HboardKey.SPACE)
        bind(keyboardView, R.id.key_backspace, HboardKey.BACKSPACE)
        bind(keyboardView, R.id.key_enter, HboardKey.ENTER)
        bind(keyboardView, R.id.key_h, HboardKey.H)
        bindImageButton(keyboardView, R.id.key_mic, HboardKey.MIC)
        bindImageButton(keyboardView, R.id.key_settings, HboardKey.SETTINGS)
        return keyboardView
    }

    fun bind(keyboardView: View, id: Int, key: HboardKey) {
        keyboardView.findViewById<MaterialButton>(id).setOnClickListener {
            onInput(key)
        }
    }
    fun bindImageButton(keyboardView: View, id: Int, key: HboardKey) {
        keyboardView.findViewById<ImageButton>(id).setOnClickListener {
            onInput(key)
        }
    }

    fun getComposeView(): View {
        val view = ComposeView(this).apply {
            setViewCompositionStrategy(
                ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed
            )
            id = R.id.hboard_compose_view
            // ...

            setContent {
                Text("This is a Compose view")
            }
        }
        return view
    }

    fun onInput(key: HboardKey) {
        val ic = currentInputConnection ?: return
        when (key) {
            HboardKey.BACKSPACE -> {
                val selectedText = ic.getSelectedText(0)
                if (TextUtils.isEmpty(selectedText)) {
                    // no selection, so delete previous character
                    ic.deleteSurroundingText(1, 0)
                } else {
                    // delete the selection
                    ic.commitText("", 1)
                }
            }
            HboardKey.H -> ic.commitText("h", 1)
            HboardKey.SPACE -> ic.commitText(" ", 1)
            HboardKey.ENTER -> {
                // 10 = enter
                ic.commitText(10.toChar().toString(), 1)
            }
            HboardKey.MIC -> {
                Toast.makeText(applicationContext, getString(R.string.mic_dialog), Toast.LENGTH_SHORT).show()
            }
            HboardKey.SETTINGS -> {
                val intent = Intent(this, MainActivity::class.java)
                intent.addFlags(FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            }
        }
    }
}